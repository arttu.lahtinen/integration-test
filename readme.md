# Integration test

## Installation steps

1. Clone repository `git clone`
2. Install dependencies
3. Start the app

## Terminal commands

```sh
git clone https://gitlab.com/arttu.lahtinen/integration-test
cd ./integration-test
npm i
```
Start server: `node src/server.js`

Run tests: `npm test`

Converting rgb to hex example: `http://localhost:3000/rgb-to-hex?red=255&green=0&blue=255`

Converting hex to rgb example: `http://localhost:3000/hex-to-rgb?hex=ff00ff`
